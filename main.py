 
import pycom
import machine
from machine import SD, Pin, Timer, RTC
import os
from microGPSd import MicroGPSd
import _thread
from uart_interface import UARTInterface, UARTMsg
import uart_interface
import time
from io import IO
#import copy

pycom.heartbeat(False)

throughput = 0
gpsd = None
uart_iface = None
sd = None
log_fname = 'no_gps_time'
rtc = None
io = None
uart_msg_fmt = None
ctl_msg_fmt = UARTMsg(uart_interface.ctl_msg)

mr_state = []

test_mode = 0
test_w1 = 2

TEST_MODE_FIXED = 0
TEST_MODE_DECAY = 1
TEST_MODE_ADAPTV = 2

TEST_W1_000 = 0
TEST_W1_025 = 1
TEST_W1_050 = 2
TEST_W1_075 = 3
TEST_W1_100 = 4

TEST_MODE_STR = ['fixed', 'decay', 'adptv']
TEST_W1_STR = ['0.00', '0.25', '0.50', '0.75', '1.00']

test_running = False
last_led = 0
led_blink = False
led_blink_c = 0xFF0000
led_fixed_c = 0
led_blink_c_lock = _thread.allocate_lock()
led_fixed_c_lock = _thread.allocate_lock()
test_running_lock = _thread.allocate_lock()

mr_state_lock = _thread.allocate_lock()


def handle_led():
    global last_led, led_blink, led_blink_c, led_fixed_c, led_fixed_c_lock, led_blink_c_lock
    
    while True:
        if (led_blink):
            led_blink_c_lock.acquire()
            pycom.rgbled(led_blink_c)
            led_blink_c_lock.release()
            time.sleep(0.05)
            led_blink_c_lock.acquire()
            pycom.rgbled(led_fixed_c)
            led_blink_c_lock.release()
            led_blink = False
        else:
            led_fixed_c_lock.acquire()
            pycom.rgbled(led_fixed_c)
            led_fixed_c_lock.release()
            time.sleep(1)
            led_blink = True


def set_led_blink_green():
    global led_blink_c, led_blink_c_lock
    led_blink_c_lock.acquire()
    if (led_blink_c is not 0x00FF00):
        led_blink_c = 0x00FF00
    led_blink_c_lock.release()

def set_led_blink_red():
    global led_blink_c, led_blink_c_lock
    led_blink_c_lock.acquire()
    if (led_blink_c is not 0xFF0000):
        led_blink_c = 0xFF0000
    led_blink_c_lock.release()

def check_gps_fix(alarm):
    global rtc
    #coord = l76.coordinates(debug=False)
    
    #print(coord)
    if (gpsd.get_fix()):
        set_led_blink_green()
        #print(gpsd.get_loc())
        if rtc is None:
            print('gps datetime: ', gpsd.datetime)
            if len(gpsd.datetime[0]) == 3 and len(gpsd.datetime[1]) == 3:
                rtc = RTC()
                rtc.init((gpsd.datetime[0][2]+2000, gpsd.datetime[0][1], gpsd.datetime[0][0], gpsd.datetime[1][0], gpsd.datetime[1][1], int(gpsd.datetime[1][2]), 0))
                print('RTC has been set!')
    else:
        set_led_blink_red()

def get_dt_str(test_mode_str, test_w1_str):
    dt = rtc.now()
    dt_str = 'test_%s_%s_%02d%02d%4d_%02d%02d.csv' % (test_mode_str, test_w1_str, dt[2], dt[1], dt[0], dt[3], dt[4])

    return dt_str

def main_thread(arg):
    global rtc, mr_state, mr_state_lock
    '''
    while True:
        print('main thread updating display')
        io.update_display(rtc, [0,0], [0,0], 0)
        print('main thread updated display, sleeping 1s')
        #time.sleep(0.5)
        print('main thread awake again')
    '''
    mr_state_lock.acquire()
    mr_state_copy = mr_state
    mr_state_lock.release()
    if len(mr_state_copy) == 8:
        io.update_display(rtc, [mr_state_copy[0], mr_state_copy[1], mr_state_copy[2]], [mr_state_copy[3], mr_state_copy[4], mr_state_copy[5]], mr_state_copy[6], test_running, TEST_MODE_STR[test_mode], TEST_W1_STR[test_w1], mr_state_copy[7])
    

def print_uart_msg(msg):
    for d in msg.keys():
        #print(d, msg[d], int.from_bytes(msg[d], 'big', True))
        msg[d] = int.from_bytes(msg[d], 'big', True)

    print(msg)

def rx_status_msg(msg):
    #print('rx msg')
    for d in msg.keys():
        msg[d] = int.from_bytes(msg[d], 'big', True)

    #io.update_display(rtc, [msg['wifi_link'], msg['wifi_rssi'], msg['wifi_pwr']], [msg['lora_link'], msg['lora_rssi'], msg['lora_pwr']], 2)
    update_mr_state([msg['wifi_link'], msg['wifi_rssi'], msg['wifi_pwr']], [msg['lora_link'], msg['lora_rssi'], msg['lora_pwr']], msg['active_radio'], None)

def test_save_log_data(msg):
    test_running_lock.acquire()
    if test_running:
        test_running_lock.release()

        for d in msg.keys():
            msg[d] = int.from_bytes(msg[d], 'big', True)

        coords = gpsd.get_loc()
        log_file = open('/sd/'+log_fname, 'a')
        log_file.write('%d,%d,%.4f,%s,%d,%.4f,%s,%d,%d,%d,%d,%d,%d,%d\n' % (msg['timestamp'],
                                                                    coords[0],
                                                                    coords[1],
                                                                    coords[2],
                                                                    coords[3],
                                                                    coords[4],
                                                                    coords[5],
                                                                    msg['wifi_link'],
                                                                    msg['lora_link'],
                                                                    msg['wifi_rssi'],
                                                                    msg['lora_rssi'],
                                                                    msg['wifi_pwr'],
                                                                    msg['lora_pwr'],
                                                                    msg['active_radio']))

        log_file.close()
        #print('log file written to')
        #io.update_display(rtc, [msg['wifi_link'], msg['wifi_rssi'], msg['wifi_pwr']], [msg['lora_link'], msg['lora_rssi'], msg['lora_pwr']], msg['active_radio'])
        update_mr_state([msg['wifi_link'], msg['wifi_rssi'], msg['wifi_pwr']], [msg['lora_link'], msg['lora_rssi'], msg['lora_pwr']], msg['active_radio'], msg['timestamp'])
        return
    else:
        test_running_lock.release()

def update_mr_state(wifi_status, lora_status, active_radio, timestamp):
    global mr_state, mr_state_lock
    #print('update mr state')
    mr_state_lock.acquire()
    mr_state = (wifi_status[0], wifi_status[1], wifi_status[2], lora_status[0], lora_status[1], lora_status[2], active_radio, timestamp)
    mr_state_lock.release()


def toggle_test():
    global test_running, test_running_lock, button, led_fixed_c, led_fixed_c_lock, test_mode, TEST_MODE_STR, test_w1, TEST_W1_STR
    global log_fname
    
    #print('btn')
    test_running_lock.acquire()
    if (test_running):
        test_running_lock.release()

        led_fixed_c_lock.acquire()
        led_fixed_c = 0
        led_fixed_c_lock.release()

        test_running_lock.acquire()
        test_running = False
        test_running_lock.release()

        uart_iface.tx_uart_msg(ctl_msg_fmt.build_msg(cmd=uart_interface.MSG_STOP, data=-1))
        uart_iface.msg_rx_cb = rx_status_msg

    else:
        test_running_lock.release()

        led_fixed_c_lock.acquire()
        led_fixed_c = 0x0000FF
        led_fixed_c_lock.release()
        
        test_running_lock.acquire()
        test_running = True
        test_running_lock.release()

        start_msg = uart_interface.MSG_START_FIXED
        if test_mode == TEST_MODE_FIXED:
            start_msg = uart_interface.MSG_START_FIXED
        elif test_mode == TEST_MODE_DECAY:
            start_msg = uart_interface.MSG_START_DECAY
        elif test_mode == TEST_MODE_ADAPTV:
            start_msg = uart_interface.MSG_START_ADAPTV

        uart_iface.tx_uart_msg(ctl_msg_fmt.build_msg(cmd=start_msg, data=test_w1))
        uart_iface.msg_rx_cb = test_save_log_data
        if gpsd.get_fix():
            log_fname = get_dt_str(TEST_MODE_STR[test_mode], TEST_W1_STR[test_w1])
        else: log_fname = 'no_gps_fix'

def toggle_mode():
    global test_mode
    #print('btn mode')
    if test_mode == 0:
        test_mode = 1
    elif test_mode == 1:
        test_mode = 2
    elif test_mode == 2:
        test_mode = 0

def toggle_test_w1():
    global test_w1
    print('btn w1', test_w1)
    if test_w1 == 0:
        test_w1 = 1
    elif test_w1 == 1:
        test_w1 = 2
    elif test_w1 == 2:
        test_w1 = 3
    elif test_w1 == 3:
        test_w1 = 4
    elif test_w1 == 4:
        test_w1 = 0

def init():
    global gpsd, uart_iface, sd, io, uart_msg_fmt

    io = IO(toggle_test, toggle_mode, toggle_test_w1)
    
    gpsd = MicroGPSd(uart=1)
    uart_msg_fmt = UARTMsg(uart_interface.min_log_msg)
    uart_iface = UARTInterface(uart_msg_fmt, receiver=True, uart_iface=2, baudrate=115200, msg_rx_cb=rx_status_msg)
    sd = SD(0)
    os.mount(sd, '/sd')

    Timer.Alarm(check_gps_fix, 2, periodic=True)
    Timer.Alarm(main_thread, 1, periodic=True)
    #_thread.start_new_thread(main_thread, ())
    _thread.start_new_thread(handle_led, ())
    
init()
