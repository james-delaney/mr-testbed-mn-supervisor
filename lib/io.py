
from machine import Pin, I2C, Timer
from ssd1306 import SSD1306_I2C
import time

i2c_m = None
ssd1306 = None

class IO():

    def __init__(self, btn_s_cb, btn_m_cb, btn_w_cb):
        self.i2c_m = None
        self.ssd1306 = None
        self.btn_s = Pin('P14', mode=Pin.IN, pull=Pin.PULL_UP)
        self.btn_s.callback(Pin.IRQ_RISING, self.handle_btn)
        self.btn_s_cb = btn_s_cb

        self.btn_m = Pin('P15', mode=Pin.IN, pull=Pin.PULL_UP)
        self.btn_m.callback(Pin.IRQ_RISING, self.handle_btn)
        self.btn_m_cb = btn_m_cb

        self.btn_w = Pin('P16', mode=Pin.IN, pull=Pin.PULL_UP)
        self.btn_w.callback(Pin.IRQ_RISING, self.handle_btn)
        self.btn_w_cb = btn_w_cb

        self.i2c_m = I2C(0, I2C.MASTER, baudrate=100000, pins=("P9", "P10"))

        self.init_display()


    def init_display(self):
        self.ssd1306 = SSD1306_I2C(128, 64, self.i2c_m)
        self.ssd1306.fill(0)
        self.ssd1306.show()
        self.ssd1306.text("MR Client", 0, 0)
        self.ssd1306.show()

    def update_display(self, rtc, wifi_status, lora_status, active_radio, run_status, mode, w1, timestamp):
        #print('updating display')

        #print('getting curr dt')
        if rtc is not None:
            dt = rtc.now()
        else:
            dt = None
        wifi_link = wifi_status[0]
        wifi_rssi = wifi_status[1]
        wifi_pwr = wifi_status[2]
        lora_link = lora_status[0]
        lora_rssi = lora_status[1]
        lora_pwr = lora_status[2]

        #print('clear display')
        self.ssd1306.fill(0)

        #print('title')
        if run_status == 0:
            self.ssd1306.text('MR Client-stop', 0, 0)
        elif run_status == 1:
            self.ssd1306.text('MR Client-run', 0, 0)


        #print('datetime')
        if dt is not None:
            #print(dt)
            self.ssd1306.text('%02d/%02d/%4d-%02d:%02d' % (dt[2], dt[1], dt[0], dt[3], dt[4]), 0, 9)

        #print('link stats')
        if wifi_link:
            self.ssd1306.text('WiFi: %d, %d' % (wifi_rssi-65535, wifi_pwr), 0, 17)
        else:
            self.ssd1306.text('WiFi: DN', 0, 16)

        if lora_link:
            self.ssd1306.text('LoRa: %d, %d' % (lora_rssi-65535, lora_pwr), 0, 25)
        else:
            self.ssd1306.text('LoRa: DN', 0, 25)

        if active_radio == 0:
            self.ssd1306.text('Sel: LoRa', 0, 33)
        elif active_radio == 1:
            self.ssd1306.text('Sel: WiFi', 0, 33)
        else:
            self.ssd1306.text('Sel: None', 0, 33)
        '''
        if mode == 0:
            self.ssd1306.text('M: fixed, %s' % (w1), 0, 41)
        elif mode == 1:
            self.ssd1306.text('M: decay, %s' % (w1), 0, 41)
        elif mode == 2:
            self.ssd1306.text('M: adaptv, %s' % (w1), 0, 41)
        '''
        self.ssd1306.text('M:%s, %s' % (mode, w1), 0, 41)

        if timestamp is not None:
            self.ssd1306.text('s: %d' % (timestamp), 0, 49)

        #print('display show')
        self.ssd1306.show()
        #print('display shown...')

    def disable_btn_irq(self, btn):
        btn.callback(Pin.IRQ_RISING, None)

    def enable_btn_irq(self, btn):
        btn.callback(Pin.IRQ_RISING, self.handle_btn)

    '''
    def handle_btn(self, arg):
        self.disable_btn_irq()
        time.sleep_ms(25)
        if self.btn_s.value():
            self.btn_s_cb()
        self.enable_btn_irq()
    '''
    def handle_btn(self, arg):
        self.disable_btn_irq(arg)
        time.sleep_ms(25)
        if arg.value():
            #print('handle btn', arg.id())
            if arg.id() == 'P14':
                self.btn_s_cb()
            elif arg.id() == 'P15':
                self.btn_m_cb()
            elif arg.id() == 'P16':
                self.btn_w_cb()
        self.enable_btn_irq(arg)