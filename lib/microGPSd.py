from micropyGPS import MicropyGPS
from machine import I2C
from machine import UART
import _thread
import time


class MicroGPSd():

    GPS_I2CADDR = const(0x10)
    

    def __init__(self, coords_update_rate=1, uart=None, pytrack=None, sda_pin='P21', scl_pin='P22'):
        
        if (uart==None):
            if (pytrack is not None):
                self.i2c = pytrack.i2c
            else:
                self.i2c = I2C(0, mode=I2C.MASTER, pins=(sda_pin, scl_pin))

            reg = bytearray(1)
            self.i2c.writeto(0x10, reg)

            self._read = self._read_i2c
        else:
            self.gps_uart = UART(uart)
            self.gps_uart.init(9600,  bits=8,  parity=None,  stop=1, pins=('P19', 'P20'))
            self._read = self._read_uart


        self.gps = MicropyGPS(local_offset=11, location_formatting='dd')

        self._coords_lock = _thread.allocate_lock()
        self._coords_lock.acquire()
        self.coords = []
        self.datetime = [(), ()]
        self._coords_lock.release()

        self.coords_update_rate = coords_update_rate
        print('starting microGPSd thread')
        _thread.start_new_thread(self._gps_thread, ())
        _thread.start_new_thread(self._gps_loc_thread, ())


    def _read_i2c(self):
        """read the data stream form the gps"""
        # Changed from 64 to 128 - I2C L76 says it can read till 255 bytes
        reg = bytearray(255)
        try:
            a = self.i2c.readfrom_into(self.GPS_I2CADDR, reg)
        except:
            pass
        return reg

    def _read_uart(self):
        reg = bytearray(255)
        if (self.gps_uart.any()):
            #return int.from_bytes((self.gps_uart.read(1)),'big')
            reg = self.gps_uart.read(255)
        
        return reg
        
    
    def _gps_thread(self):
        while True:
            data = self._read()
            
            for i in range(len(data)):
                self.gps.update(chr(data[i]))
            time.sleep(0.5)

    
    def _gps_loc_thread(self):
        while True:
            if (self.get_fix()):
                self._coords_lock.acquire()
                self.coords = [self.gps._latitude[0], self.gps._latitude[1], self.gps._latitude[2], self.gps._longitude[0], self.gps._longitude[1], self.gps._longitude[2]]
                self.datetime = [self.gps.date, self.gps.timestamp]
                self._coords_lock.release()
            else:
                self._coords_lock.acquire()
                self.coords = [0, 0.0, 'N', 0, 0.0, 'E']
                self.datetime = [(0, 0, 0), (0,0,0)]
                self._coords_lock.release()
            
            time.sleep(self.coords_update_rate)
            
        

    def get_fix(self):
        if (self.gps.time_since_fix() > 0):
            return True
        else:
            return False
        
    def get_loc(self):
        '''
        coords = []
        self._coords_lock.acquire()
        coords = self.coords
        self._coords_lock.release()
        '''
        return self.coords
    