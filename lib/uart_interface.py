from machine import UART
import _thread
import time

sample_log_msg = {  'timestamp':        4,
                    'wifi_link':        1,
                    'lora_link':        1,
                    'wifi_rssi':        2,
                    'lora_rssi':        2,
                    'active_radio':     1,
                    'lat':              9,
                    'lon':              9,
}

min_log_msg = (     ('timestamp',        4,),
                    ('wifi_link',        1,),
                    ('lora_link',        1,),
                    ('wifi_rssi',        2,),
                    ('lora_rssi',        2,),
                    ('wifi_pwr',         1,),
                    ('lora_pwr',         1,),
                    ('active_radio',     1,),
                    ('ind', b'\xff\xaa\xff')
)

ctl_msg = (         ('cmd',              1,),
                    ('data',             1,),
                    ('ind', b'\xff\xbb\xff',),
)

MSG_START           = 0
MSG_STOP            = 1
MSG_START_FIXED     = 2
MSG_START_DECAY     = 3
MSG_START_ADAPTV    = 4


class UARTMsg():

    def __init__(self, msg_struct):
        self.msg_struct = msg_struct
        self.msg_ind = None
        self.msg_len = 0

        for l in msg_struct:
            if (l[0] is 'ind'):
                self.msg_ind = l[1]
            else:
                self.msg_len += l[1]
            #self.log_dict[l[0]] = l[1]


    def len(self):
        return self.msg_len

    def build_msg(self, **kwargs):
        msg_bytes = bytearray(self.msg_ind)
        for d, d_len in self.msg_struct:
            if d is not 'ind':
                try:
                    #msg_bytes.append(int(kwargs.pop(d)).to_bytes(d_len, 'big'))
                    a = int(kwargs.pop(d)).to_bytes(d_len, 'big')
                    #print(d, d_len, a)
                    msg_bytes.extend(a)
                except KeyError:
                    print('UARTMsg.build_msg got non-existent var: ', d)

        msg_bytes.extend(self.msg_ind)
        return msg_bytes

class UARTInterface():

    def __init__(self, msg_format=None, receiver=False, uart_iface=1, baudrate=115200, msg_rx_cb=None):
        
        self.msg_format = msg_format
        self.uart = None
        self.uart_iface = uart_iface
        self.baudrate = baudrate

        self.msg_rx_cb = msg_rx_cb

        self.rx_queue = []
        self.tx_queue = []
        self.rx_queue_lock = _thread.allocate_lock()
        self.tx_queue_lock = _thread.allocate_lock()

        self.uart = UART(self.uart_iface, baudrate=self.baudrate, parity=None, bits=8, stop=1, pins=('P3','P11'))

        _thread.start_new_thread(self._uart_thread, ())
        _thread.start_new_thread(self._uart_rx_thread, ())

    def tx_uart_msg(self, log_msg):
        
        self.tx_queue_lock.acquire()
        #print('queue uart log msg')
        #print(log_msg)
        self.tx_queue.append(log_msg)
        self.tx_queue_lock.release()


    def _uart_thread(self):
        
        while True:
            #print('logging rx thread tick')
            d = self.uart.read(34)
            if d is not None:
                # block and wait unconditionally to get rx lock if we have data that might get lost
                #self.rx_queue_lock.acquire(waitflag=1, timeout=-1)
                
                self.rx_queue_lock.acquire()
                self.rx_queue.append(d)
                self.rx_queue_lock.release()

            self.tx_queue_lock.acquire()
            #print('try tx uart log msg')
            if len(self.tx_queue) > 0:
                #print('sending uart log msg')
                self.uart.write(self.tx_queue.pop(0))
            self.tx_queue_lock.release()

            time.sleep(0.005)

    def _uart_rx_thread(self):

        while True:
            self.rx_queue_lock.acquire()
            if len(self.rx_queue) > 0:
                #print('uart rx')
                d = self.rx_queue.pop(0)
                self._proc_rx_msg(d)
            self.rx_queue_lock.release()
            time.sleep(0.005)



    def _proc_rx_msg(self, data):
        #print('proc uart data ', len(data))
        msg = None
        if data[0:3] == self.msg_format.msg_ind:
            #print('proc uart data ', data[len(data)-3:len(data)])
            if data[len(data)-3:len(data)] == self.msg_format.msg_ind:
                msg = data[3:len(data)-3]
                #print(['%x'%(l) for l in msg])
                
                idx = 0
                msg_d = {}
                if len(msg) == self.msg_format.msg_len:
                    for l in self.msg_format.msg_struct:
                        if l[0] is not 'ind':
                            msg_d[l[0]] = msg[idx:idx+l[1]]         # put corresponding no. of bytes for msg data point into msg_d
                            #print(l[0], idx, idx+l[1], ['%x'%(l) for l in msg[idx:idx+l[1]]])
                            idx += l[1]
                else:
                    return None
                
                #print('calling uart cb')
                self.msg_rx_cb(msg_d)
                #print('called uart cb')
            else:
                return None
        else:
            return None
        
        #print('return _proc_rx_msg')
        return
